# Hacklabor CTF Snippets

## Grundlegendes
- Wenn du nicht weiter kommst, zögere nicht, jemand Anderes zu fragen
- Arbeite mit Tmux - [https://tmuxcheatsheet.com/](https://tmuxcheatsheet.com/)
- WriteUps erst veröffentlichen, wenn die Challenge vorbei ist

## Youtube
- IppSec [https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA](https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA)
- John Hammond - [https://www.youtube.com/user/RootOfTheNull](https://www.youtube.com/user/RootOfTheNull)
- LiveOverflow - [https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w)


## Zum Lernen
- Allgemein
    - [https://ctftime.org/](https://ctftime.org/)
    - [https://www.hackthebox.eu/](https://www.hackthebox.eu/ "https://www.hackthebox.eu/")
    - [https://tryhackme.com/](https://tryhackme.com/) Anfänger - Fortgeschrittene; Verschiedene Kategorien; teilweise geführt durch Fragen die beantwortet werden müssen; writeups vorhanden auch während der Zeit wo die Challenges aktiv sind (gut zum lernen)
- Web
    - Anfänger:
        - [http://overthewire.org/wargames/natas/](http://overthewire.org/wargames/natas/)
- Machines
    - Anfänger:
        - [http://overthewire.org/wargames/bandit/](http://overthewire.org/wargames/bandit/)


### Dinge die man gehört haben sollte
- Heartbleed
    - Metasploit
- DirtyCow
- Reverse Shell
    - http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet
    - On Machine
        - Spawn https://netsec.ws/?p=337
        - Full Shell https://forum.hackthebox.eu/discussion/142/obtaining-a-fully-interactive-shell
    
- SQL Injection
- XSS
- ROP (return orientet Programming) [https://youtu.be/8Dcj19KGKWM](https://youtu.be/8Dcj19KGKWM)

## Softwaretools & Libarys


### dnSpy (Windows)
 [dnsSpy GIT](https://github.com/0xd4d/dnSpy "snSpy Git")

   Disassembler/Discompiler/Debugger für .net Anwendungen 

### gdb (Linux)
 Disassembler/Debugger
[gdb install](https://www.tutorialspoint.com/gnu_debugger/installing_gdb.htm)

cmdline debugger; 
mit [Peda Plugin](https://github.com/longld/peda "Peda Plugin") noch besser
plugin

### radare2 (Windows, linux)
 Disassembler/Debugger
[radare2/Cutter](https://rada.re/n/)

änlich wie gdb ein cmd Tool hat aber schon eine sehr einfache CMD "GUI"

mit **Cutter** ( Disassembler/Discompiler/Debugger) gib es eine vollwertige GUI dafür. Debugger ist beta und fühlt sich auch so an. Cutter nutzt sowohl radare2 wie auch Ghidra

### IDA64 (windows/mac/linux)
professioneller Disasembler/Discompiler/Debugger
[ida64](https://www.hex-rays.com/)
ist aber auch in einer free Version verfügbar. Discompiler ist dann aber nicht enthalten.

### Ghidra (Windows/Mac/Linux)
von der NSA Disasembler/Discompiler [https://ghidra-sre.org/](https://ghidra-sre.org/)


### Hopper (Linux / Mac)
Disassembler/Discompiler/Debugger [hopper Webseite](https://www.hopperapp.com/)

Vollversion einmalig 99€ --> funktioniert bei mir bis jetzt am besten (THK)

### JohnThe
[https://www.openwall.com/john/](https://www.openwall.com/john/)
passwd cracker von Private keys bis keepass
John the Ripper is an Open Source password security auditing and password recovery tool available for many operating systems. John the Ripper jumbo supports hundreds of hash and cipher types, including for: user passwords of Unix flavors (Linux, *BSD, Solaris, AIX, QNX, etc.), macOS, Windows, "web apps" (e.g., WordPress), groupware (e.g., Notes/Domino), and database servers (SQL, LDAP, etc.); network traffic captures (Windows network authentication, WiFi WPA-PSK, etc.); encrypted private keys (SSH, GnuPG, cryptocurrency wallets, etc.), filesystems and disks (macOS .dmg files and "sparse bundles", Windows BitLocker, etc.), archives (ZIP, RAR, 7z), and document files (PDF, Microsoft Office's, etc.) These are just some of the examples - there are many more.

[https://www.hackingarticles.in/beginner-guide-john-the-ripper-part-1/](https://www.hackingarticles.in/beginner-guide-john-the-ripper-part-1/)


[https://www.hackingarticles.in/beginners-guide-for-john-the-ripper-part-2/](https://www.hackingarticles.in/beginners-guide-for-john-the-ripper-part-2/)
#### Hydra (HTTP/FTP dict Attack)
- https://null-byte.wonderhowto.com/how-to/hack-like-pro-crack-online-web-form-passwords-with-thc-hydra-burp-suite-0160643/
#### cmdline use 

```bash
# HTTP Post FORM
# -l coolerName / -L list.txt - bestimmter Name oder Liste
# -p tollesPasswort / -l password.txt - bestimmtest Passwort oder Liste
# -s PORT
# -m index.php(Datei an die gesendet wird):(Trenner)user(PostField)=^USER^(Platzhalter Username)&(HTTP next Field)password(PostField)=^PASS^(Platzhalter Passwort):invalid(Text der Fehler anzeigt; hier wird geprüft, ob der Login fehlgeschlagen wurde)
# -t THREADS
# hostname - ersetzen mit Domain (hacklabor.de/ctf/login.php) -> hacklabor.de; /ctf/login.php wird unter -m angegeben

hydra -l admin -P ./rockyou.txt -u -s 80 -m '/index.php:user=^USER^&password=^PASS^:invalid' -t 10 hostname http-post-form 
```


#### Hexedit

```bash
sudo apt install hexedit
```

einfacher cmdl Hex Editor Linux

#### Password Listen
- Sammlung 
    -   https://github.com/danielmiessler/SecLists/tree/master/Passwords
- Empfehlung
    - RockYou "wget https://github.com/danielmiessler/SecLists/blob/master/Passwords/Leaked-Databases/rockyou.txt.tar.gz?raw=true"

#### Cyberchef
[https://gchq.github.io/CyberChef/](https://gchq.github.io/CyberChef/)

! from qchq

The Cyber Swiss Army Knife - a web app for encryption, encoding, compression and data analysis.

#### fcrackzip

 	sudo apt install fcrackzip
 	fcrackzip -u -v -D -p wordlist-dict.txt example.zip
 	fcrackzip -u -v -l 1-6 -c aA1 example.zip 
 	Switch Explanation:
 	-v : verbose output, display the progress of current crack, may slow the progress a little bit
 	-l : length of password to brute-force in this case (1 to 6 characters)
 	-c : character set to try (a – lower-alphabet, A-uppercase alphabet, 1-numeric, ! – include [!:$%&/()=?[]+*~#])
 	-u : verify the zip password in case of multiple possible matches

#### XOR Cracker

[https://wiremask.eu/tools/xor-cracker/](https://wiremask.eu/tools/xor-cracker/)

#### hashcat (gpucluster01)

[https://hashcat.net/hashcat/](https://hashcat.net/hashcat/)

#### Autopsy (Forensic)

*.E01 files

[https://www.autopsy.com/](https://www.autopsy.com/)

#### volatility (Forensic)

*.mem files

[Autopsy Pulgin](https://medium.com/@markmckinnon_80619/volatility-autopsy-plugin-module-8beecea6396)

[https://github.com/volatilityfoundation/volatility](https://github.com/volatilityfoundation/volatility)



#### Sonic Visulayzer (Forensic)

[https://www.sonicvisualiser.org/](https://www.sonicvisualiser.org/)

#### Audacity (Sound Forensic)

[https://www.audacityteam.org/](https://www.audacityteam.org/)

## Python Librarys

### angr & claripy

Automated REVERS Brutforce Flags as Input to binaries

[JH explain angr example](https://youtu.be/RCgEIBfnTEI)

### pwntools

  [pwntools](http://docs.pwntools.com/en/latest/)

[JH explain pwntools + Rop example](https://youtu.be/i5-cWI_HV8o)

python libary; wird meistens für Bufferoverflows und ROP bzw 2libc angriffe auf binarys benutzt, um eine Reverse Shell zu bekommen.

### sympy

automatische Lösung von Gleichungen
[JH explain sympy example](https://youtu.be/UYd86e5bt3E)


## Payloads

### Nützliche XSS Payloads
- [Payload all the things](https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/XSS%20injection), große Auswahl an verschiedenen XSS Demopayloads
- [XSS Payloads ohne Eventhandler (z.B. 'onerror', ...)](https://brutelogic.com.br/blog/xss-without-event-handlers/)
- Bypass bracket/parentheses filter
```php+HTML
alert`1`
setTimeout`alert\u0028document.domain\u0029`;
<img src="fileThatDoesNotExist" onerror="javascript:window.onerror=alert;throw 'XSS'" dummyParam="" >
```

### SQL Injection

[https://github.com/payloadbox/sql-injection-payload-list](https://github.com/payloadbox/sql-injection-payload-list)


### ASM Shellcode

[http://shell-storm.org/](http://shell-storm.org/)

### Reverse Shell

[https://weibell.github.io/reverse-shell-generator/](https://weibell.github.io/reverse-shell-generator/)

## Images

### Bilder Header als Batch bearbeiten

```python
import numpy as np
from PIL import Image

def modify_file(filename):
    with open(filename, "rb") as f:
        with open(filename.replace("jpg", "png"), "wb") as g:
            image = f.read()
            
            image2 = bytes.fromhex("89504E470D0A1A0A")
            g.write(image2 + image[10:])
```


​	
​	
```python
data = np.zeros((2000,2000,4), dtype=np.uint8)

for i in range(100):
    for j in range(100):
        #modify_file("flag_{}_{}.jpg".format(i,j))
        im = Image.open("flag_{}_{}.png".format(i,j))
        array = np.array(im.getdata()).reshape(20,20,4)
        data[20*i:20*(i+1), 20*j:20*(j+1)] = array

img = Image.fromarray(data, 'RGBA')
img.save('flag.png')
```


- 89504E470D0A1A0A Magic numbers für PNG 
- ersten 10 Zeichen werden gegen die von PNG getauscht

### suche im I-Net

bei Yadex und Bing kann man auch nur nach Bildausschnitten suchen. Dagegen ist Google Bildersuche gut wenn man einen Namen für das man sieht haben will z.B. Typ einer Brücke auf dem Bild. 

- [Image search Yandex](https://yandex.com/images/) (Russland)
- [image search BING](https://www.bing.com/?scope=images&nr=1&FORM=NOFORM)
- [image search Google](https://www.google.com/imghp?hl=EN)
- [https://tineye.com/](https://tineye.com) scheint so eine Personen Bild suche zu sein (it scars me)
- [NASA Satelite images over time; Wayback; Timemachine](https://worldview.earthdata.nasa.gov/)

# Linux

um Dateien zu finden die als root aufrufbar sind 

```find / -perm -u=s -type f
find / -perm -u=s -type f
```

## linPEAS

script um nach privilege escalation zu suchen

[https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite)	

```
wget https://raw.githubusercontent.com/carlospolop/privilege-escalation-awesome-scripts-suite/master/linPEAS/linpeas.sh
```



# Stego

Verpixelte Schrift

[https://github.com/beurtschipper/Depix](https://github.com/beurtschipper/Depix)





## Links

[https://github.com/JohnHammond/ctf-katana](https://github.com/JohnHammond/ctf-katana) more ctf hints

[https://archive.org/](https://archive.org/) Wayback Machine; Internetarchive

[https://github.com/apsdehal/awesome-ctf](https://github.com/apsdehal/awesome-ctf)



https://libc.rip/

#### TODO
- https://github.com/codingo/Reconnoitre