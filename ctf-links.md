# CTF

### CRYPTO

[#1 BEST - AES Encryption Function ontools | #1 BEST - AES Encryption Free Online Tool Supports 128 192 256 Bits... | #1 BEST - AES Encryption Online - ᐈ AES Encryption and Decryption Online Tool | Online AES Encryption and Decryption function ontools](https://www.aesencryptiononline.com/2022/03/aes-encryption-function-ontools.html)

[Caesar Cipher Decoder (online tool) | Boxentriq](https://www.boxentriq.com/code-breaking/caesar-cipher#tool)

### CTFGAMES

[pwnable.kr/play.php](http://pwnable.kr/play.php)

[The Legend of the Headless Horseman](https://solvers.battelle.org/cyber-challenge/headless-horseman)

### DEVELOPER

[Free Online Tools For Developers - to Beautify, Validate, Minify, Analyse, Convert JSON, XML, JavaScript, CSS, HTML, Excel](https://codebeautify.org/?__cf_chl_captcha_tk__=684740f715b78deec8c1e114b24aae5b27040890-1600534576-0-AZfgsFBxF5qB9yXOSB7apS6j_2KKG5tPuWCpo04mfFM12a9tekTEXjgA_IlNSheBGN_8URbfrrnA4k9e0SbZsKYNwQokzU6s3vHlXu7pZCv1bP5wT8kBVkdeOtGuL90qEN8SQy0Su8GwXTnQ205pNPAK8LWlkcFt-j0sPHJELhuQ0nL9fBAE9_za0FzDvBW7b8IBGVuC3zTRxAr1WjnkV7VoaNCvMydNL6h-aZbLOTqR6lOcTjBrFOKATmh4MRwmdU39Gu8imS35nr-6FKjqv5FZaZkWdjHt5JcFBLd67_Khj4xfPT88O-6t4039cnkvNgd-AxPm0W-ig1nep_cVK4OSY50EF1UY2sVGr0DOnxFgSfpWO7dA05ZUH8eY7yPIaWklYraafb5ZxbnuOzqJ3yFj2rQebZkNeP5RMVZOxMDYWPFdoad9cGIPu6XEhNqCCEXhr2f9yVBULc_lkRZEgBFnry0y0ROc_IHxhQGyoupo)

### FOTO\_STEGO\_PDF

[Post-Apocalyptic Images of Japan - GakuranmanGakuranman](http://gakuran.com/post-apocalyptic-images-of-japan/)

[beurtschipper/Depix: Recovers passwords from pixelized screenshots](https://github.com/beurtschipper/Depix)

[Forensically, free online photo forensics tools - 29a.ch](https://29a.ch/photo-forensics/#error-level-analysis)

[Aperi'Solve](https://aperisolve.fr/)

[FotoForensics - Analysis](http://fotoforensics.com/analysis.php?id=080d24dfcd8220e2daa962fcec644ea862ef54df.1072818)

[Steganography Online](https://stylesuxx.github.io/steganography/)

[Steganographic Decoder](https://futureboy.us/stegano/decinput.html)

[PDF Extract Tool](https://www.pdf-online.com/osa/extract.aspx)

### MULTITOOLS\_SITES

[Rawsec's CyberSecurity Inventory](https://inventory.rawsec.ml/tools.html#title-tools-osint)

[XOR Brute Force - CyberChef](https://gchq.github.io/CyberChef/#recipe=XOR_Brute_Force(null,100,0,'Standard',false,true,false,'')&input=NjY)

[GitHub - JohnHammond/ctf-katana: This repository aims to hold suggestions (and hopefully/eventually code) for CTF challenges. The "project" is nicknamed Katana.](https://github.com/JohnHammond/ctf-katana)

[ZXing Barcode Decoder Online](https://zxing.org/w/decode.jspx)

[CrackStation - Online Password Hash Cracking - MD5, SHA1, Linux, Rainbow Tables, etc.](https://crackstation.net/)

[rmusser01/Infosec\_Reference: An Information Security Reference That Doesn't Suck; https://rmusser.net/git/admin-2/Infosec\_Reference for non-MS Git hosted version.](https://github.com/rmusser01/Infosec_Reference)

[SCADA Hacking: The Key Differences between Security of SCADA and Traditional IT systems](https://www.hackers-arise.com/post/2017/11/27/SCADA-Hacking-The-difference-between-Security-of-SCADA-systems-and-Traditional-IT-systems)

[Geocaching / MysteryMaster - TOOL ZUR UNTERSTÜTZUNG BEIM LÖSEN VON RÄTSEL-CACHES (MYSTERIES)](https://gps-cache.de/geocaching/mystery-master/mysterymaster.htm)

### OSINT

[Have I Been Pwned: Check if your email has been compromised in a data breach](https://haveibeenpwned.com/)

[OSINT - start.me](https://start.me/p/ZME8nR/osint)

[WhatsMyName Web](https://whatsmyname.app/)

[Open Location Code - Wikipedia](https://en.wikipedia.org/wiki/Open_Location_Code#Example)

[OSINT/GEOINT - Investigating and geolocating a commercial flight • Haax - Personal Blog](https://haax.fr/en/writeups/osint-geoint/osint-flight-tracking-challenge/)

[SunCalc - sun position, sunlight phases, sunrise, sunset, dusk and dawn times calculator](http://suncalc.net/#/49.6722,3.0244,7/2020.08.30/18:10)

[Online converter to all coordinate systems | UTM, WGS.. | with map](https://coordinates-converter.com/en/decimal/51.000000,10.000000?karte=OpenStreetMap&zoom=8)

[Downloads](chrome://downloads/)

[Epieos, the ultimate OSINT tool](https://epieos.com/)

[mxrch/GHunt: 🕵️‍♂️ Offensive Google framework.](https://github.com/mxrch/GHunt)

[Blog | hackers-arise](https://www.hackers-arise.com/)
[_IntelligenceX](https://intelx.io/) schaut auch unter Tools

### PAYLOAD

[GitHub - swisskyrepo/PayloadsAllTheThings: A list of useful payloads and bypass for Web Application Security and Pentest/CTF](https://github.com/swisskyrepo/PayloadsAllTheThings)

### PWN\_REVERSE

[Anti Debugging Protection Techniques with Examples](https://www.apriorit.com/dev-blog/367-anti-reverse-engineering-protection-techniques-to-use-before-releasing-software)

[KosBeg/z3\_staff: My small script with useful functions for typical Z3-rev CTF tasks.](https://github.com/KosBeg/z3_staff)

[Writing Exploits with Pwntools Tutorial](https://tc.gts3.org/cs6265/2019/tut/tut03-02-pwntools.html)

[GitHub - Gallopsled/pwntools-tutorial: Tutorials for getting started with Pwntools](https://github.com/Gallopsled/pwntools-tutorial)

### SM\_SITES

[@DanFlashes Instagram profile with posts and stories - Picuki.com](https://www.picuki.com/profile/DanFlashes)

[IMSTACLONE](https://dumpor.com/search?query=Vividpineconepig)

### WEB

[JWT Vulnerabilities (Json Web Tokens) - HackTricks](https://book.hacktricks.xyz/pentesting-web/hacking-jwt-json-web-tokens)

[CSV Plot - Online CSV plots](https://www.csvplot.com/)

[Markdown Guide](https://www.markdownguide.org/basic-syntax/)

[Zahlensystem-Rechner](https://manderc.com/concepts/umrechner/index.php)



