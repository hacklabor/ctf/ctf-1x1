


### CRYPTO

[#1 BEST - AES Encryption Function ontools | #1 BEST - AES Encryption Free Online Tool Supports 128 192 256 Bits... | #1 BEST - AES Encryption Online - ᐈ AES Encryption and Decryption Online Tool | Online AES Encryption and Decryption function ontools](https://www.aesencryptiononline.com/2022/03/aes-encryption-function-ontools.html)

[Caesar Cipher Decoder (online tool) | Boxentriq](https://www.boxentriq.com/code-breaking/caesar-cipher#tool)

[Password Hash Cracking in Amazon Web Services | SANS Institute](https://www.sans.org/blog/password-hash-cracking-amazon-web-services/#_10)

### CTFGAMES

[pwnable.kr/play.php](http://pwnable.kr/play.php)

[The Legend of the Headless Horseman](https://solvers.battelle.org/cyber-challenge/cyber-challenge)

[Michael's OSINT CTF](https://ctf.michweb.de/scoreboard)

### DEVELOPER

[Free Online Tools For Developers - to Beautify, Validate, Minify, Analyse, Convert JSON, XML, JavaScript, CSS, HTML, Excel](https://codebeautify.org/?__cf_chl_captcha_tk__=684740f715b78deec8c1e114b24aae5b27040890-1600534576-0-AZfgsFBxF5qB9yXOSB7apS6j_2KKG5tPuWCpo04mfFM12a9tekTEXjgA_IlNSheBGN_8URbfrrnA4k9e0SbZsKYNwQokzU6s3vHlXu7pZCv1bP5wT8kBVkdeOtGuL90qEN8SQy0Su8GwXTnQ205pNPAK8LWlkcFt-j0sPHJELhuQ0nL9fBAE9_za0FzDvBW7b8IBGVuC3zTRxAr1WjnkV7VoaNCvMydNL6h-aZbLOTqR6lOcTjBrFOKATmh4MRwmdU39Gu8imS35nr-6FKjqv5FZaZkWdjHt5JcFBLd67_Khj4xfPT88O-6t4039cnkvNgd-AxPm0W-ig1nep_cVK4OSY50EF1UY2sVGr0DOnxFgSfpWO7dA05ZUH8eY7yPIaWklYraafb5ZxbnuOzqJ3yFj2rQebZkNeP5RMVZOxMDYWPFdoad9cGIPu6XEhNqCCEXhr2f9yVBULc_lkRZEgBFnry0y0ROc_IHxhQGyoupo)

### FOTO_STEGO_PDF

[beurtschipper/Depix: Recovers passwords from pixelized screenshots](https://github.com/beurtschipper/Depix)

[Forensically, free online photo forensics tools - 29a.ch](https://29a.ch/photo-forensics/#error-level-analysis)

[Aperi'Solve](https://aperisolve.fr/)

[FotoForensics - Analysis](http://fotoforensics.com/analysis.php?id=080d24dfcd8220e2daa962fcec644ea862ef54df.1072818)

[Steganography Online](https://stylesuxx.github.io/steganography/)

[Steganographic Decoder](https://futureboy.us/stegano/decinput.html)

[PDF Extract Tool](https://www.pdf-online.com/osa/extract.aspx)

[PDF Extractor](https://www.extractpdf.com/de.html)

[ctf-tools/stegsolve/install at master · zardus/ctf-tools · GitHub](https://github.com/zardus/ctf-tools/blob/master/stegsolve/install)

### MULTITOOLS_SITES

[Rawsec's CyberSecurity Inventory](https://inventory.rawsec.ml/tools.html#title-tools-osint)

[XOR Brute Force - CyberChef](https://gchq.github.io/CyberChef/#recipe=XOR_Brute_Force(null,100,0,'Standard',false,true,false,'')&input=NjY)

[GitHub - JohnHammond/ctf-katana: This repository aims to hold suggestions (and hopefully/eventually code) for CTF challenges. The "project" is nicknamed Katana.](https://github.com/JohnHammond/ctf-katana)

[CrackStation - Online Password Hash Cracking - MD5, SHA1, Linux, Rainbow Tables, etc.](https://crackstation.net/)

[ZXing Barcode Decoder Online](https://zxing.org/w/decode.jspx)

[rmusser01/Infosec_Reference: An Information Security Reference That Doesn't Suck; https://rmusser.net/git/admin-2/Infosec_Reference for non-MS Git hosted version.](https://github.com/rmusser01/Infosec_Reference)

[SCADA Hacking: The Key Differences between Security of SCADA and Traditional IT systems](https://www.hackers-arise.com/post/2017/11/27/SCADA-Hacking-The-difference-between-Security-of-SCADA-systems-and-Traditional-IT-systems)

[Geocaching / MysteryMaster - TOOL ZUR UNTERSTÜTZUNG BEIM LÖSEN VON RÄTSEL-CACHES (MYSTERIES)](https://gps-cache.de/geocaching/mystery-master/mysterymaster.htm)

### OSINT

[Have I Been Pwned: Check if your email has been compromised in a data breach](https://haveibeenpwned.com/)

[OSINT - start.me](https://start.me/p/ZME8nR/osint)

[WhatsMyName Web](https://whatsmyname.app/)

[Open Location Code - Wikipedia](https://en.wikipedia.org/wiki/Open_Location_Code#Example)

[SunCalc - sun position, sunlight phases, sunrise, sunset, dusk and dawn times calculator](http://suncalc.net/#/49.6722,3.0244,7/2020.08.30/18:10)

[Online converter to all coordinate systems | UTM, WGS.. | with map](https://coordinates-converter.com/en/decimal/51.000000,10.000000?karte=OpenStreetMap&zoom=8)

[Epieos, the ultimate OSINT tool](https://epieos.com/)

[mxrch/GHunt: 🕵️‍♂️ Offensive Google framework.](https://github.com/mxrch/GHunt)

[Blog | hackers-arise](https://www.hackers-arise.com/)

[Macau - The World Factbook](https://www.cia.gov/the-world-factbook/countries/macau/)

[Echtzeit Blitzkarte :: LightningMaps.org](https://www.lightningmaps.org/?lang=de#m=oss;t=3;s=0;o=0;b=0.00;ts=0;z=11;y=53.6055;x=11.3705;d=2;dl=2;dc=0;)

[PimEyes: Face Recognition Search Engine and Reverse Image Search](https://pimeyes.com/en)

[TinEye Reverse Image Search](https://tineye.com/)

[WiGLE: Wireless Network Mapping](https://www.wigle.net/)

[Intelligence X](https://intelx.io/)

[GeoHints](https://geohints.com/)

[GeoGuessr- The Top Tips, Tricks and Techniques | The Digital Labyrinth](https://somerandomstuff1.wordpress.com/2019/02/08/geoguessr-the-top-tips-tricks-and-techniques/)

[License Plates of the World](http://www.worldlicenseplates.com/)

[GeoTips – Tips and tricks for Geoguessr](https://geotips.net/)

### mail

[Free Tools to optimize your Email Marketing Data](https://www.emailsherlock.com/tools)

[Email Permutator | Metric Sparrow Toolkit](http://metricsparrow.com/toolkit/email-permutator/)

[recruitment.camp • Hunter](https://hunter.io/try/search/recruitment.camp?locale=en)

[Find email addresses in seconds • Hunter (Email Hunter)](https://hunter.io/)

[WearBlackAllDay/SeedCandy: A selection of useful tools regarding SeedFinding, packed in an (hopefully) accessible UI.](https://github.com/WearBlackAllDay/SeedCandy)

[#govbins](https://govbins.uk/)

[Draw Radius Map | Measure Circle Google Map | Google Map Radius Calculator](https://www.maps.ie/draw-radius-circle-map/)

[Warenkorb Ihrer MYPOSTER Bestellung](https://www.myposter.de/warenkorb)

[2023/challenge/challenge.ino · master · hacklabor / Technolympiade · GitLab](https://gitlab.com/hacklabor/technolympiade/-/blob/master/2023/challenge/challenge.ino?ref_type=heads)

[OSINT/GEOINT - Investigating and geolocating a commercial flight • Haax - Personal Blog](https://web.archive.org/web/20230206131550/https://haax.fr/en/writeups/osint-geoint/osint-flight-tracking-challenge/)

[Digital Journalism | OSINT Essentials](https://www.osintessentials.com/)

### PAYLOAD

[GitHub - swisskyrepo/PayloadsAllTheThings: A list of useful payloads and bypass for Web Application Security and Pentest/CTF](https://github.com/swisskyrepo/PayloadsAllTheThings)

### PWN_REVERSE

[Anti Debugging Protection Techniques with Examples](https://www.apriorit.com/dev-blog/367-anti-reverse-engineering-protection-techniques-to-use-before-releasing-software)

[KosBeg/z3_staff: My small script with useful functions for typical Z3-rev CTF tasks.](https://github.com/KosBeg/z3_staff)

[Writing Exploits with Pwntools Tutorial](https://tc.gts3.org/cs6265/2019/tut/tut03-02-pwntools.html)

[GitHub - Gallopsled/pwntools-tutorial: Tutorials for getting started with Pwntools](https://github.com/Gallopsled/pwntools-tutorial)

[pics/wasm.png at master · corkami/pics](https://github.com/corkami/pics/blob/master/binary/wasm.png)

[Stack | Binary Exploitation](https://ir0nstone.gitbook.io/notes/types/stack)

[CTF/pwn/binary_exploitation_101/10-bypassing_canaries/fuzz.py at main · Crypto-Cat/CTF](https://github.com/Crypto-Cat/CTF/blob/main/pwn/binary_exploitation_101/10-bypassing_canaries/fuzz.py)

### SM_SITES

[@DanFlashes Instagram profile with posts and stories - Picuki.com](https://www.picuki.com/profile/DanFlashes)

[IMSTACLONE](https://dumpor.com/search?query=Vividpineconepig)

### WEB

[JWT Vulnerabilities (Json Web Tokens) - HackTricks](https://book.hacktricks.xyz/pentesting-web/hacking-jwt-json-web-tokens)

[GitHub - gwen001/gwen001](https://github.com/gwen001/gwen001)

[offsec.tools - A vast collection of security tools](https://offsec.tools/)

### wireshark/tshark

[tshark tutorial and filter examples | HackerTarget.com](https://hackertarget.com/tshark-tutorial-and-filter-examples/)

[CSV Plot - Online CSV plots](https://www.csvplot.com/)

[Markdown Guide](https://www.markdownguide.org/basic-syntax/)

[Zahlensystem-Rechner](https://manderc.com/concepts/umrechner/index.php)

### JAVA

[skylot/jadx: Dex to Java decompiler](https://github.com/skylot/jadx)

[frida/frida: Clone this repo to build Frida](https://github.com/frida/frida)

[CTF_WriteUps/UMDCTF2023.md at master · kush412/CTF_WriteUps · GitHub](https://github.com/kush412/CTF_WriteUps/blob/master/UMDCTF2023/UMDCTF2023.md)

[entpixelsoftware](https://github.com/megvii-research/NAFNet)

### STENO

[Demo | Open Steno Project](https://www.openstenoproject.org/demo/)

[StegOnline](https://www.georgeom.net/StegOnline/upload)