import requests
import json
import sys
import os
import secrets


def req(url):



    payload={}
    headers = {
  'authority': 'ctftime.org',
  'cache-control': 'max-age=0',
  'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="99", "Google Chrome";v="99"',
  'sec-ch-ua-mobile': '?0',
  'sec-ch-ua-platform': '"Windows"',
  'dnt': '1',
  'upgrade-insecure-requests': '1',
  'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36',
  'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
  'sec-fetch-site': 'none',
  'sec-fetch-mode': 'navigate',
  'sec-fetch-user': '?1',
  'sec-fetch-dest': 'document',
  'accept-language': 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7',
  'cookie': 'csrftoken=e6qDd9d5YXFIKgWaJzfxP7nYqqxAUbW5; cookieconsent_status=dismiss; sessionid=d49e7191f1352ed35a3a67b58b9ab2ce; cf_chl_2=f8230f80935a19d; cf_chl_prog=x11; cf_clearance=YCML8EK0DyYAZrPniMR2Em4C8FwG4YTbTH_FlRdLZX8-1646379270-0-150; ss="eyJ1c2VybmFtZSI6ICJUaDBtNHNLIiwgImlkIjogODc2MjYsICJ0cyI6IDE2NDYzNzkyODh9:cf1ca19c8a5ffb21aef08c397acf08e488e46d3c7a66550157d4f4a9143c022a"; pt=gAAAAABiIcEYtEdZSI7BW309pVZ5EsL3rACl-Hj8gWF_hlnvC84aArUoHdsSwxrFMXpkaljoq1C6olwzXqaI_gRTkLyDk138ZA96Sf_JcEXR4VAdjgjYLgbh_QjDU4jRJqNO_-WQoqDo9ABeIwf65dsF7_nPtRiusl4xWl6Hw4e6iBD1JcD_HNnpwHLauCKLchelv3UwmdW32BO87_9y2hSPmpKyyTLdXnPLlIpikVRT-bziUU8vNh7On1wEeahms6ZmCIpf0yUGxticDbie0zr8IQ68qGQnYvWgZM3zcMHtYHXnOpI7GBL1XUiS8BWs8ZqdeNfM_GDV3BxmEMZ8rGMLNOawD7lwuoaNIsAbMEdS7uOcx7r8chYHJHqIBetu2f8JXiS78lWS'
}

    print(url)

    response = requests.request("GET", url, headers=headers, data=payload)
    print(response)
    
    return response.text 

def addToClipBoard(text):
    command = 'echo | set /p nul=' + text.strip() + '| clip'
    os.system(command)

def generate_temp_password(length):
    if not isinstance(length, int) or length < 8:
        raise ValueError("temp password must have positive length")

    chars = "abcdefghijklmnopqrstuvwABCDEFGHJKLMNPQRSTUVWXYZ23456789"
    from os import urandom

    # original Python 2 (urandom returns str)
    # return "".join(chars[ord(c) % len(chars)] for c in urandom(length))

    # Python 3 (urandom returns bytes)
    return "".join(chars[c % len(chars)] for c in urandom(length))

number = sys.argv[1]+"/"
res=req("https://ctftime.org/api/v1/events/"+number)
     
y = json.loads(res)
print(y)
pr=""
pr+=y["title"]+"   weight: "+str(y["weight"])+"\r\n"
pr+='URL: '+y["url"]+''+"\r\n"
pr+="USER: Hacklabor"+"\r\n"
pr+="MAIL: inbox-"+y["title"].replace(" ","_")+'@cyber42.de'+"\r\n"
pr+="Passwort: "+generate_temp_password(16)+"\r\n"
pr+=""
pr+="\r\nNextcloud Zugang zum Austausch der Dateien und Gedanken\r\n\r\n"
pr+="URL: "+secrets.nextcloudurl+"\r\n"
pr+="User: "+secrets.nextclouduser+   "\r\n"
pr+="PW: "+secrets.nextcloudpass+   "\r\n"
pr+="DokuTool: https://obsidian.md/ !ihr könnt aber auch jedes andere Markdown Tool nehmen.\r\n\r\n\r\n"
import datetime
d1 = datetime.datetime.fromisoformat(y["start"])
d1=d1.replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)
d2 = datetime.datetime.fromisoformat(y["finish"])
d2=d2.replace(tzinfo=datetime.timezone.utc).astimezone(tz=None)
pr+="Start: "+str(d1)+"\r\n"
pr+="Ende : "+str(d2)+"\r\n"
pr+="Tage: "+ str(y["duration"]["days"])+"  Stunden: "+str( y["duration"]["hours"])+"\r\n"
pr+="ctfTime URL: "+y["ctftime_url"]+"\r\n\r\n"

pr+=y["description"].replace("\r\n\r\n","\r\n")+"\r\n"


print(pr)


