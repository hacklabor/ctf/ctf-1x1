import requests
import json
import sys
import os



def req(url):



    payload={}
    headers = {
    'authority': 'ctftime.org',
    'cache-control': 'max-age=0',
    'sec-ch-ua': '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'dnt': '1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'none',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'accept-language': 'de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7',
    
    }

    response = requests.request("GET", url, headers=headers, data=payload)

    
    return response.text 

def addToClipBoard(text):
    command = 'echo | set /p nul=' + text.strip() + '| clip'
    os.system(command)

def generate_temp_password(length):
    if not isinstance(length, int) or length < 8:
        raise ValueError("temp password must have positive length")

    chars = "abcdefghijklmnopqrstuvwABCDEFGHJKLMNPQRSTUVWXYZ23456789"
    from os import urandom

    # original Python 2 (urandom returns str)
    # return "".join(chars[ord(c) % len(chars)] for c in urandom(length))

    # Python 3 (urandom returns bytes)
    return "".join(chars[c % len(chars)] for c in urandom(length))
teamId = sys.argv[1]
number = sys.argv[2]
weight = sys.argv[3]
res=req("https://ctftime.org/api/v1/results/")
y = json.loads(res)
#print(y)
li= y[str(number)]
score= li['scores']
maxTeams=len(score)
ctf=li['title']

maxPoints=score[0]['points'].split('.')
maxPoints=maxPoints[0]
#print (li)
for db in score:

    if int(db['team_id'])==int(teamId):
        place=db['place']
        points=db['points'].split('.')
        points=points[0]
res=req("https://ctftime.org/api/v1/teams/"+str(teamId)+'/')
y = json.loads(res)
TeamName = y['name']
print ('CTF   :',ctf,' Weight',weight)
print ('Team  :', TeamName, ' Platz:',str(place)+'/'+str(maxTeams))
print ('Points:',str(points)+'/'+str(maxPoints))
points_coif=float(points)/float(maxPoints)
place_coif=1.0/float(place)
E_rating=((place_coif+points_coif)*float(weight))#/(1.0/(1+place/maxTeams))
print(E_rating)





