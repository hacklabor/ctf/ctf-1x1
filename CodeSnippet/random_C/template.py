from pwn import *
from subprocess import Popen, PIPE

p = remote("spaceheroes-atm.chals.io", 443, ssl=True, sni="spaceheroes-atm.chals.io")
print(p.recvuntil("Option: "))
p.sendline("w")
print(p.recvuntil("Amount: "))
process = Popen(["./c/a.out", "."], stdout=PIPE)
(output, err) = process.communicate()
print (output)

exit_code = process.wait()
p.sendline(output)
print(p.recv())
p.interactive()