#!!/bin/bash

while [[ true ]]; do
    if [[ -f flag.txt ]] 
    then
        file="flag.txt"
        sign=`xxd flag.txt | head -n 1 | awk '{print $2}'`
    else
        file="flag"
        sign=`xxd flag | head -n 1 | awk '{print $2}'`
    fi
    if [[ $sign == "fd37"  ]]
    then
        mv $file flag.7z
        p7zip -d flag.7z
    elif [[ $sign == "1f8b" ]]
    then
        mv $file flag.gz
        gzip -d flag.gz
    elif [[ $sign == "504b" ]]
    then
        mv $file flag.zip
        unzip flag.zip
        rm flag.zip
    elif [[ $sign == "425a" ]]
    then
        mv $file flag.bz2
        bzip2 -d flag.bz2
    else
        cat flag | base64 -d
        exit 1
    fi
done
