# gdb commands


|               |                                                          |     |
| ------------- | -------------------------------------------------------- | --- |
| `aslr`        | Inspect or modify ASLR status.                           |     |
| `checksec`    | Print out the binary security settings using `checksec`. |     |
| `elfheader`   | Print the section mappings contained in the ELF header.  |     |
| `hexdump`     | Hex-dump data at the specified address (or at `$sp`).    |     |
| `main`        | GDBINIT compatibility alias for `main` command.          |     |
| `nearpc`      | Disassemble near a specified address.                    |     |
| `nextcall`    | Break at the next call instruction.                      |     |
| `nextjmp`     | Break at the next jump instruction.                      |     |
| `nextjump`    | Break at the next jump instruction.                      |     |
| `nextret`     | Break at next return-like instruction.                   |     |
| `nextsc`      | Break at the next syscall not taking branches.           |     |
| `nextsyscall` | Break at the next syscall not taking branches.           |     |
| `pdisass`     | Compatibility layer for PEDA's pdisass command.          |     |
| `procinfo`    | Display information about the running process.           |     |
| `regs`        | Print out all registers.                                 |     |
| `stack`       | Print dereferences of stack data.                        |     |
| `search`      | Search memory for bytes, strings, pointers, or integers. |     |
| `telescope`   | Recursively dereference pointers.                        |     |
| `vmmap`       | Print virtual memory map pages.                          |     |
| n             | stepp Over                                               |     |
| s             | stepp into                                               |     |
| c             | run to next Breakpoint continue                          |     |
| r             | run Programm                                             |     |
| r < payload   | r and pipe  payload File in                              |     |
| b             | Breakpoint   exp b main oder b*$40042                    |     |
| clear         | delete Breakpoint                                        |     |
| info break    | list breakpoints                                         |     |

## [Ghidra: static analyzer / decompiler](https://tc.gts3.org/cs6265/tut/tut02-warmup2.html#ghidra-static-analyzer--decompiler)



# Tools

## Read Formatstring

liest 100 x Formatstring Positionen aus


```
python3 readPointer_fomatstr.py <binary> <target_string> <format>"

binary = ELF
target_string = <send Payload after>  hier :
format= ausleseformat (x=hex llx=long hex d=dez lld=long dez s=string p=pointer )
```

![](ctf-1x1/CodeSnippet/pwn/read_fomatstr.py)

![](Pasted%20image%2020240529201139.png)